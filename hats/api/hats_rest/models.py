from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveIntegerField()
    shelf_number = models.PositiveIntegerField()

class Hat(models.Model):
    style = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    fabric = models.CharField(max_length=50)
    picture_url = models.URLField(blank=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})
