import React, {useState, useEffect} from 'react';

function ShoeForm() {
  const [bins, setBins] = useState([])
  const [FormData, setFormData] = useState({
      model: "",
      manufacturer: "",
      color: "",
      picture_url: "",
      bin: ""
  })

  const getData = async () => {
    const url = "http://localhost:8100/api/bins/";
    const responce = await fetch(url);

    if (responce.ok) {
        const data = await responce.json();
        setBins(data.bins)
    }
}

useEffect(() => {
  getData();
}, []);

const handleSubmit = async (event) => {
  event.preventDefault();
  console.log(FormData)
  const shoeUrl = "http://localhost:8080/api/shoes/";

  const fetchConfig = {
      method: "POST",
      body: JSON.stringify(FormData),
      headers: {
          "Content-Type": "application/json",
      },
  };

  const responce = await fetch(shoeUrl, fetchConfig);

  if (responce.ok) {
      setFormData({
          model: "",
          manufacturer: "",
          color: "",
          picture_url: "",
          bin: ""
      });
  }
}



const handleFormChange = (e) => {
  const value = e.target.value;
  const inputName = e.target.name;
  setFormData({
      ...FormData,
      [inputName]: value
  });
}



return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={FormData.model} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="name">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={FormData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="ends">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={FormData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="starts">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={FormData.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" alt="emptystring" className="form-control" />
              <label htmlFor="max_presentations">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={FormData.bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;