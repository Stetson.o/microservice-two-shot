import React, {useState, useEffect} from 'react';

function HatForm() {
    const [locations, setLocations] = useState([])
    const [FormData, setFormData] = useState({
        style: "",
        color: "",
        fabric: "",
        picture_url: "",
        location: "",
    })

    const getData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const responce = await fetch(url);

        if (responce.ok) {
            const data = await responce.json();
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log(FormData)
        const hatUrl = "http://localhost:8090/api/hats/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(FormData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const responce = await fetch(hatUrl, fetchConfig);

        if (responce.ok) {
            setFormData({
                style: "",
                color: "",
                fabric: "",
                picture_url: "",
                location: "",
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                  <label htmlFor="name">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="starts">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="ends">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.picture_url} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" alt="emptystring" className="form-control" />
                  <label htmlFor="max_presentations">Picture Url</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleFormChange} value={FormData.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>{location.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}
export default HatForm;
