import React, {useState, useEffect } from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState([])

    const getData = async () => {
        const responce = await fetch("http://localhost:8080/api/shoes/");

        if (responce.ok) {
            const data = await responce.json();
            setShoes(data.shoes)
        }
    }


    useEffect(()=>{
        getData();
    }, [])


    const handleDelete = ((id) => {
        fetch("http://localhost:8080/api/shoes/"+id,{method: "DELETE"}).then(() => {
            window.location.reload()
        })
    });

    return (
        <>
        <table>
          <thead>
            <tr>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>Color</th>
              <th>Bin</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.model }</td>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.color }</td>
                  <td>{ shoe.bin.closet_name }</td>
                  <td><button onClick={() => handleDelete(shoe.id)}>Delete</button></td>
                </tr>

              );
            })}
          </tbody>
        </table>
        </>
      );
}

export default ShoeList;