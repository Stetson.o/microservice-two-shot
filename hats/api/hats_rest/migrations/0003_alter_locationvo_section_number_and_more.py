# Generated by Django 4.0.3 on 2024-01-31 18:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_rename_name_locationvo_closet_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='locationvo',
            name='section_number',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='shelf_number',
            field=models.CharField(max_length=100),
        ),
    ]
