import React, {useState, useEffect} from 'react';

function HatsList() {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const responce = await fetch("http://localhost:8090/api/hats/");

        if (responce.ok) {
            const data = await responce.json();
            setHats(data.hats)
        }
    }

    useEffect(()=>{
        getData();
    }, [])

    const handleDelete = ((id) => {
        fetch("http://localhost:8090/api/hats/"+id,{method: "DELETE"}).then(() => {
            window.location.reload()
        })
    });


    return (
        <>
        <table>
          <thead>
            <tr>
              <th>Style</th>
              <th>Color</th>
              <th>Fabric</th>
              <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
              return (
                <tr key={hat.id}>
                  <td>{ hat.style }</td>
                  <td>{ hat.color }</td>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.location.closet_name }</td>
                  <td><button onClick={() => handleDelete(hat.id)}>Delete</button></td>
                </tr>

              );
            })}
          </tbody>
        </table>
        </>
      );
}

export default HatsList;
